#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* gcc -O0 -fno-stack-protector lab1.c */

int authenticate(char *buf) {
	
	int authenticated = 0;
	char password[252];

	/* Copy over the password */
	strcpy(password, buf);

	for (int i = 0; i < 8; i++) {
	   password[i] ^= 0x0a;
	}

	/* Compare against the password */
	if(strcmp(buf, "\x7a\x6b\x79\x79\x3b\x38\x39\x3a") == 0) {
		authenticated = 1;
	}

	return authenticated;	
}

int main (){
   char buf[1000] = {0};

   printf("\n\nPassword: ");
   scanf("%1000s", buf);
   if (authenticate(buf)){
      printf("\n\nAuthenticated!\n");
   } else {
      printf("\n\nFailed Password...\n");
   }

   return 0;
}
